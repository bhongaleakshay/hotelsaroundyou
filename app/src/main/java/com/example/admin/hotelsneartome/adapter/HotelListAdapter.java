package com.example.admin.hotelsneartome.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.admin.hotelsneartome.R;
import com.example.admin.hotelsneartome.model.Photo;
import com.example.admin.hotelsneartome.model.Result;
import com.example.admin.hotelsneartome.utils.Utils;

import java.util.ArrayList;
import java.util.List;


public class HotelListAdapter extends RecyclerView.Adapter<HotelListAdapter.HotelListViewHolder> {



    private Context context;

    private ArrayList<Result> mList;


    public HotelListAdapter(Context context,
                            ArrayList<Result> mListl) {
        this.context = context;
        this.mList = mListl;
    }

    @Override
    public HotelListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.adapter_location, parent, false);
        return new HotelListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HotelListViewHolder holder, int i) {
        String name=mList.get(i).getName();
        String address=mList.get(i).getVicinity();
        if(!Utils.isEmpty(name))
        {
            holder.mtxtHotelName.setText(name);
        }

        if(!Utils.isEmpty(address))
        {
            holder.mtxtAddress.setText(address);
        }

        if(mList.get(i).getRating()!=null) {
            holder.mtxtRating.setRating(mList.get(i).getRating().floatValue());
        }

        loadImageUrl(holder,mList.get(i).getPhotos());

    }

    private void loadImageUrl(HotelListViewHolder holder,List<Photo> mPhotoList)
    {
        try
        {
            if(mPhotoList!=null)
            {
                if(mPhotoList.size()>0)
                {
                    String reference=mPhotoList.get(0).getPhotoReference();
                    int width=mPhotoList.get(0).getWidth();
                    int height=mPhotoList.get(0).getHeight();
                    if(!Utils.isEmpty(reference)&& width>0 && height>0)
                    {
                        Utils.loadImageUrl(context,holder.imageView,reference,height,width);
                    }
                    else {
                        holder.imageView.setImageDrawable(null);
                    }
                }
                else {
                    holder.imageView.setImageDrawable(null);
                }
            }
            else {
                holder.imageView.setImageDrawable(null);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void updateList(ArrayList<Result> results)
    {
        mList=results;
        notifyDataSetChanged();
    }
    @Override
    public int getItemCount() {
        return mList.size();
    }


    class HotelListViewHolder extends RecyclerView.ViewHolder {

        private TextView mtxtHotelName;
        private TextView mtxtAddress;
        private RatingBar mtxtRating;
        private ImageView imageView;
        public HotelListViewHolder(View itemView) {
            super(itemView);
           mtxtHotelName=itemView.findViewById(R.id.txtHotelName);
           mtxtAddress=itemView.findViewById(R.id.txtAddress);
           mtxtRating=itemView.findViewById(R.id.txtRating);
           imageView=itemView.findViewById(R.id.imageView);
        }
    }


}