package com.example.admin.hotelsneartome;

import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import com.example.admin.hotelsneartome.webservice.CloudHelper;

import retrofit2.Retrofit;

public class HotelsAroundMeApp extends MultiDexApplication {

    @Override
    public void onCreate() {
        super.onCreate();
        MultiDex.install(this);
        Retrofit dpRetrofit= CloudHelper.getRetrofit(
                CloudHelper.getOkHttpDebugClient()
        );

        CloudHelper.init(dpRetrofit);
    }
}
