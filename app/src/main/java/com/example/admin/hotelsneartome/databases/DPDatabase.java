package com.example.admin.hotelsneartome.databases;

import android.arch.persistence.db.SupportSQLiteOpenHelper;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.DatabaseConfiguration;
import android.arch.persistence.room.InvalidationTracker;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.support.annotation.NonNull;

import com.example.admin.hotelsneartome.utils.DbConstants;

/**
 * Database Class
 */
@Database(entities = {LocationInfo.class}
,version = DbConstants.DB_VERSION,exportSchema = false)
public abstract class DPDatabase extends RoomDatabase {


    public abstract LocationDao dpDao();

    private static DPDatabase INSTANCE;

    private static String TAG=DPDatabase.class.getCanonicalName();


    @NonNull
    @Override
    protected SupportSQLiteOpenHelper createOpenHelper(DatabaseConfiguration config) {
        //Logger.d(TAG,"createOpenHelper");
        return null;
    }

    static DPDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (DPDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            DPDatabase.class, DbConstants.DB_NAME)
                            .fallbackToDestructiveMigration()
                            /*.allowMainThreadQueries()
                            .addCallback(new Callback() {
                                @Override
                                public void onCreate(@NonNull SupportSQLiteDatabase db) {
                                    super.onCreate(db);
                                    //Logger.d(TAG,"onCreateDBCalled");
                                    new PopulateDbAsync(INSTANCE).execute();
                                }
                            })*/
                            .build();
                }
            }
        }
        return INSTANCE;
    }

   /* private static class PopulateDbAsync extends AsyncTask<Void, Void, Void> {
        private final LocationDao dpDao;


        public PopulateDbAsync(DPDatabase instance) {
            dpDao= instance.dpDao();
            //Logger.d(TAG,"Async");
        }

        @Override
        protected Void doInBackground(Void... voids) {
            //Logger.d(TAG,"doInBackground");

            if(vcfList!=null) {
                    List<LocationInfo> dbList=new ArrayList<>();
                    for(VCFResponse response:vcfList) {
                        if(response!=null) {
                            LocationInfo vcf = new LocationInfo();
                            vcf.setVc(response.getVc());
                            String json = Utils.getGson().
                                    toJson(response.getVcfConfig(), VCFConfig.class);
                            //Logger.d(TAG, "LocationInfo " + json);
                            vcf.setData(json);
                            vcf.setVersion(response.getProtoVer());
                            dpDao.insertVCF(vcf);
                            //Logger.d(TAG,"dbInsert");
                            dbList.add(vcf);
                        }
                    }
                    //Logger.d(TAG,"Size "+dbList.size());
                    //result=dpDao.insertVCFList(dbList);
                LocationInfo vcf= dpDao.getVCF("54380325R");
                //Logger.d(TAG,"LocationInfo from DB "+vcf.getVc());
            }

            return null;
        }
    }*/
    @NonNull
    @Override
    protected InvalidationTracker createInvalidationTracker() {
        //Logger.d(TAG,"invalidationTracker");
        return null;
    }

    @Override
    public void clearAllTables() {
        //Logger.d(TAG,"clearAllTables");
    }
}
