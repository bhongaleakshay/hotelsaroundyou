package com.example.admin.hotelsneartome.utils;

import android.content.Context;
import android.graphics.Typeface;

/**
 * Singleton for Typeface to set custom font in app
 */

public class FontFace {

    /**
     * single instance of class
     */
    private static FontFace fontFace;

    /**
     * type face for light font
     */
    private static Typeface fontLight;

    /**
     * type face for regular font
     */

    private static Typeface fontRegular;


    /**
     * Type face for medium font
     */
    private static Typeface fontMedium;



    private FontFace() {

    }

    /**
     * return font face instance
     *
     * @param context context of requested view
     * @return font face
     */
    public static FontFace getFontFace(Context context) {
        if(context!=null) {
            if (fontFace == null) {
                fontFace = new FontFace();
                fontRegular = Typeface.createFromAsset(context.getAssets(), AssetConstants.FONT_REGULAR);
                fontLight = Typeface.createFromAsset(context.getAssets(), AssetConstants.FONT_LIGHT);
                fontMedium = Typeface.createFromAsset(context.getAssets(), AssetConstants.FONT_MEDIUM);
            }
        }
        return fontFace;
    }

    /**
     * return light font face
     *
     * @return type face for light
     */
    public Typeface getFontLight() {
        return fontLight;
    }

    /**
     * return regular font face
     *
     * @return type face for regular
     */
    public Typeface getFontRegular() {
        return fontRegular;
    }


    /**
     * return medium font face
     *
     * @return type face for medium
     */
    public Typeface getFontMedium() {
        return fontMedium;
    }



}
