package com.example.admin.hotelsneartome.utils;

import android.content.Context;
import android.util.AttributeSet;



/**
 * this class is used to set custom font for text view
 */

public class TextViewLight extends android.support.v7.widget.AppCompatTextView {


    public TextViewLight(Context context) {
        super(context);
        initFont(context);
    }

    public TextViewLight(Context context, AttributeSet attrs) {
        super(context, attrs);
        initFont(context);
    }

    public TextViewLight(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initFont(context);
    }

    /**
     * to set custom font to textView
     *
     * @param context context of view
     */
    private void initFont(Context context) {
        setTypeface(FontFace.getFontFace(context).getFontLight());
    }
}
