package com.example.admin.hotelsneartome.webservice;


import com.example.admin.hotelsneartome.model.Example;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * To Consume Web service API
 */

public interface IWebServiceApi {



    String API_KEY="AIzaSyBnp6FyShgV5WkfZzJZAgFA3lRRBsISOUo";



    @GET("api/place/nearbysearch/json?key="+API_KEY+"&sensor=true")
    Observable<Example> getNearbyPlaces(@Query("type") String type, @Query("location") String location,
                                  @Query("radius") int radius);


}
