package com.example.admin.hotelsneartome.utils;

/**
 * AppConstants
 */

public class AppConstants {

    /**
     * Permission and intent codes
     */
    public static final int PERMISSION_CODE = 1003;

    public static final int SPLASH_TIME = 3000;

    public static final int REQUEST_CHECK_SETTINGS=1004;



    public static final int PERMISSION_SETTING = 1000;


}
