package com.example.admin.hotelsneartome.utils;

/**
 * Constants file for web service keywords
 */

public class WebApiConstants {



    public static final String BASE_URL ="https://maps.googleapis.com/maps/";


    public static final int CODE_OVER_QUERY_LIMIT =4001;

    public static final  int CODE_ZERO_RESULTS =4002;

    public static final  int CODE_REQUEST_DENIED =4003;

    public static final  int CODE_INVALID_REQUEST =4004;

    public static final  int CODE_UNKNOWN_ERROR =4005;

    public static final  int CODE_SOMETHING_WENT_WRONG =4006;


    public static final String MSG_OVER_QUERY_LIMIT="OVER_QUERY_LIMIT";

    public static final String MSG_ZERO_RESULTS="ZERO_RESULTS";

    public static final String MSG_REQUEST_DENIED="REQUEST_DENIED";

    public static final String MSG_INVALID_REQUEST="INVALID_REQUEST";

    public static final String MSG_UNKNOWN_ERROR="UNKNOWN_ERROR";

    public static final String MSG_SOMETHING_WENT_WRONG="SOMETHING_WENT_WRONG";


    public static final String STATUS_OK="OK";
}
