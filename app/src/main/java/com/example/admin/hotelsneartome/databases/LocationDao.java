package com.example.admin.hotelsneartome.databases;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

/**
 * query wrapper for room database
 */

@Dao
public interface LocationDao {



    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertLocation(LocationInfo locationInfo);

    @Query("SELECT * From locationinfo where latitude = :latitude AND longitude= :longitude")
    LocationInfo getLastSession(String latitude,String longitude);

    @Query("SELECT * FROM LocationInfo ORDER BY locationIndex DESC LIMIT 1")
    LocationInfo getAvailableData();

}
