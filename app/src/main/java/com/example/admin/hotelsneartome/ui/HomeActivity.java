package com.example.admin.hotelsneartome.ui;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.content.IntentSender;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.example.admin.hotelsneartome.R;
import com.example.admin.hotelsneartome.adapter.HotelListAdapter;
import com.example.admin.hotelsneartome.model.Result;
import com.example.admin.hotelsneartome.utils.AppConstants;
import com.example.admin.hotelsneartome.utils.DbConstants;
import com.example.admin.hotelsneartome.utils.Utils;
import com.example.admin.hotelsneartome.viewmodels.CloudDataViewModel;
import com.example.admin.hotelsneartome.viewmodels.LocationModel;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import java.util.ArrayList;

public class HomeActivity extends BaseActivity {

    private String TAG=HomeActivity.class.getCanonicalName();
    private CloudDataViewModel mCloudDataViewModel;

    private RecyclerView mRecyclerView;
    private ProgressBar mProgressBar;
    private HotelListAdapter mHotelListAdapter;
    private ArrayList<Result> mResults;

    private LocationSettingsRequest.Builder builder;

    @Override
    public void onPermissionGrant() {
            Log.d(TAG,"Home Permission Grant");
            init();
    }

    @Override
    public void playServiceNotUpdated() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        if(Utils.isPermissionAllowed(getApplicationContext())) {
           init();
        }


    }

    private void init()
    {
        Log.d(TAG,"Home init");
        initObject();
        initViews();
        mCloudDataViewModel.getResultList().observe(this, new Observer<ArrayList<Result>>() {
            @Override
            public void onChanged(@Nullable ArrayList<Result> results) {
                Log.d(TAG, "Offline Data:: onchanged ");
                mProgressBar.setVisibility(View.GONE);
                if (results != null) {
                    Log.d(TAG, "Offline Data:: onchanged update list");
                    mHotelListAdapter.updateList(results);
                }
            }
        });
        mCloudDataViewModel.getErrorCode().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(@Nullable Integer integer) {
                mProgressBar.setVisibility(View.GONE);
                if(integer!=null) {
                    try {
                        int error=integer;
                        Snackbar.make(mRecyclerView, Utils.getErrorMsg(error), Snackbar.LENGTH_SHORT)
                                    .show();
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private void initObject()
    {
        mResults=new ArrayList<>();
        initLocationSettings();
        mHotelListAdapter=new HotelListAdapter(getApplicationContext(),mResults);
        mCloudDataViewModel=ViewModelProviders.of(this).get(CloudDataViewModel.class);
       // String lat="18.3446545";
       // String lng="74.0296803";

        mCloudDataViewModel.downloadFromDb(getApplicationContext(),null,null);
    }

    private void initViews()
    {
        mProgressBar=findViewById(R.id.progressbar);
        mProgressBar.setVisibility(View.GONE);
        mRecyclerView=findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        mRecyclerView.setAdapter(mHotelListAdapter);

    }

    private void initLocationSettings()
    {
        builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(LocationModel.getInstance().getLocationRequest());
        LocationModel.getInstance().initGoogleApiClient(getApplicationContext());
        SettingsClient client = LocationServices.getSettingsClient(this);
        Task<LocationSettingsResponse> task = client.checkLocationSettings(builder.build());
        task.addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
            @Override
            public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                // All location settings are satisfied. The client can initialize
                // location requests here.
                Log.d(TAG,"on Location Setting success");

                subscribeToLocationUpdate();
            }
        });

        task.addOnFailureListener(this, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d(TAG,"on Location Setting failure");
                if (e instanceof ResolvableApiException) {
                    // Location settings are not satisfied, but this can be fixed
                    // by showing the user a dialog.
                    try {
                        // Show the dialog by calling startResolutionForResult(),
                        // and check the result in onActivityResult().
                        ResolvableApiException resolvable = (ResolvableApiException) e;
                        resolvable.startResolutionForResult(HomeActivity.this,
                                AppConstants.REQUEST_CHECK_SETTINGS);
                    } catch (IntentSender.SendIntentException sendEx) {
                        // Ignore the error.
                        sendEx.printStackTrace();
                    }
                }
            }
        });

    }


    private void subscribeToLocationUpdate() {

        LocationModel.getInstance().observe(this, new Observer<Location>() {
            @Override
            public void onChanged(@Nullable Location location) {
                Log.d(TAG, "onChanged: location updated " + location);
                // do your stuff
                if(location!=null) {
                    mProgressBar.setVisibility(View.VISIBLE);
                    if (Utils.isInternetConnected(getApplicationContext())) {
                        if(mResults==null||mResults.isEmpty()) {
                            Log.d(TAG, "onChanged: download list");
                            mCloudDataViewModel.downloadHotelsList(getApplicationContext(), location);
                        }
                        else {
                            Log.d(TAG, "onChanged: not to download");
                        }
                    } else {
                        String lat = String.valueOf(location.getLatitude());
                        String lng = String.valueOf(location.getLongitude());
                        mCloudDataViewModel.downloadFromDb(getApplicationContext(), lat, lng);
                    }
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG,"Home::onActivityResult");

        if(requestCode== AppConstants.REQUEST_CHECK_SETTINGS)
        {
            subscribeToLocationUpdate();
        }
    }
}
