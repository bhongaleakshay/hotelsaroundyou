package com.example.admin.hotelsneartome.viewmodels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.location.Location;
import android.support.annotation.NonNull;
import android.util.Log;

import com.example.admin.hotelsneartome.databases.DBHelper;
import com.example.admin.hotelsneartome.databases.LocationInfo;
import com.example.admin.hotelsneartome.model.Example;
import com.example.admin.hotelsneartome.model.Result;
import com.example.admin.hotelsneartome.utils.DbConstants;
import com.example.admin.hotelsneartome.utils.Utils;
import com.example.admin.hotelsneartome.utils.WebApiConstants;
import com.example.admin.hotelsneartome.webservice.CloudHelper;

import java.util.ArrayList;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class CloudDataViewModel extends AndroidViewModel {

    private String TAG=CloudDataViewModel.class.getCanonicalName();
    private CompositeDisposable mCompositeDisposable=new CompositeDisposable();

    private MutableLiveData<Integer> mErrorCode=new MutableLiveData<>();

    private MutableLiveData<ArrayList<Result>> mResultList=new MutableLiveData<>();

    public CloudDataViewModel(@NonNull Application application) {
        super(application);
    }

    public void downloadHotelsList(final Context context, final Location location)
    {
        Disposable disposable = CloudHelper.downloadHotels(context,"restaurant",location)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<Example>() {

                    @Override
                    public void onNext(Example example) {
                        if(example!=null) {
                            try {
                                Log.d("OnNext", "" + example.getStatus());
                                if (example.getStatus().equalsIgnoreCase(WebApiConstants.STATUS_OK)) {
                                    if (example.getResults() != null) {
                                        mResultList.postValue(new ArrayList<>(example.getResults()));
                                    }
                                } else {
                                    switch (example.getStatus())
                                    {
                                        case WebApiConstants.MSG_OVER_QUERY_LIMIT:
                                            mErrorCode.postValue(WebApiConstants.CODE_OVER_QUERY_LIMIT);
                                            break;
                                        case WebApiConstants.MSG_ZERO_RESULTS:
                                            mErrorCode.postValue(WebApiConstants.CODE_ZERO_RESULTS);
                                            break;
                                        case WebApiConstants.MSG_REQUEST_DENIED:
                                            mErrorCode.postValue(WebApiConstants.CODE_REQUEST_DENIED);
                                            break;
                                        case WebApiConstants.MSG_INVALID_REQUEST:
                                            mErrorCode.postValue(WebApiConstants.CODE_INVALID_REQUEST);
                                            break;
                                        case WebApiConstants.MSG_UNKNOWN_ERROR:
                                            mErrorCode.postValue(WebApiConstants.CODE_UNKNOWN_ERROR);
                                            break;
                                         default:
                                             mErrorCode.postValue(WebApiConstants.CODE_SOMETHING_WENT_WRONG);
                                             break;
                                    }
                                }
                            }
                            catch (Exception e)
                            {
                                e.printStackTrace();
                                mErrorCode.postValue(WebApiConstants.CODE_SOMETHING_WENT_WRONG);
                            }
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        mErrorCode.postValue(WebApiConstants.CODE_SOMETHING_WENT_WRONG);
                        if (!Utils.isEmpty(e.getMessage())) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onComplete() {

                    }
                });
        mCompositeDisposable.add(disposable);
    }


    public void downloadFromDb(Context context,String lat,String lng)
    {
        DBHelper.getDbHelper(context).getHotelList(lat,lng)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<String>() {


                    @Override
                    public void onNext(String s) {
                        if (!Utils.isEmpty(s)) {
                            Log.d(TAG,"Offline Data:: "+s);
                            LocationInfo model=Utils.getGson().fromJson(s,LocationInfo.class);
                            if(model!=null)
                            {
                                if(!Utils.isEmpty(model.getJson())) {
                                    Example example = Utils.getGson().fromJson(model.getJson(),Example.class);
                                    Log.d(TAG,"Offline Data:: json if");
                                    if(example.getResults()!=null)
                                    {
                                        Log.d(TAG,"Offline Data:: "+example.getResults().size());
                                        mResultList.postValue(new ArrayList<>(example.getResults()));
                                    }
                                }
                                else
                                {
                                    Log.d(TAG,"Offline Data:: json null");
                                    mErrorCode.postValue(DbConstants.DB_DATA_NOT_AVAILABLE);
                                }
                            }
                            else {
                                Log.d(TAG,"Offline Data:: else model ");
                                mErrorCode.postValue(DbConstants.DB_DATA_NOT_AVAILABLE);
                            }
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        mErrorCode.postValue(DbConstants.DB_DATA_NOT_AVAILABLE);
                        if (!Utils.isEmpty(e.getMessage())) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public MutableLiveData<ArrayList<Result>> getResultList() {
        return mResultList;
    }

    public MutableLiveData<Integer> getErrorCode() {
        return mErrorCode;
    }
}
