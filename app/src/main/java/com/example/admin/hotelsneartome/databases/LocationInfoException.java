package com.example.admin.hotelsneartome.databases;


public class LocationInfoException extends Exception {

    private int errorCode;

    public LocationInfoException() {
        super();
    }

    public LocationInfoException(String message, int errorCode) {
        super(message);
        this.errorCode = errorCode;
    }

    public LocationInfoException(String message, int errorCode, Throwable cause) {
        super(message, cause);
        this.errorCode = errorCode;
    }

    @Override
    public String toString() {
        return super.toString();
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }

    public int getErrorCode() {
        return errorCode;
    }

}