package com.example.admin.hotelsneartome.databases;

import android.content.Context;
import android.util.Log;

import com.example.admin.hotelsneartome.utils.DbConstants;
import com.example.admin.hotelsneartome.utils.Utils;

import java.util.concurrent.Callable;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;

/**
 * Helper Class for Database
 */

public class DBHelper {

    private LocationDao dpDao;

    private static DBHelper dbHelper;

    private static String TAG=DBHelper.class.getCanonicalName();

    private DBHelper(Context application) {
        DPDatabase dpDatabase = DPDatabase.getDatabase(application);
        dpDao = dpDatabase.dpDao();
        //setConnectionStatus();
    }

    public synchronized static DBHelper getDbHelper(Context context) {

        synchronized (DBHelper.class) {
            if (dbHelper == null) {
                dbHelper = new DBHelper(context);
            }
        }
        return dbHelper;
    }





    public void insertVCF(LocationInfo vcf) {
        long count=dpDao.insertLocation(vcf);
        Log.d(TAG,"insertVCF "+count);
    }



    public Observable<String> getHotelList(final String latitude, final String longitude) {
        return Observable.defer(new Callable<ObservableSource<? extends String>>() {
            @Override
            public ObservableSource<? extends String> call() throws Exception {
                LocationInfo locationInfo = null;
                int ERROR_CODE=0;
                String msg="No record";
                if(!Utils.isEmpty(latitude) && !Utils.isEmpty(longitude)) {
                    Log.d(TAG, "getLast HotelList "+latitude+" "+longitude);
                   locationInfo = dpDao.getLastSession(latitude, longitude);
                   if(locationInfo==null)
                   {
                       ERROR_CODE=DbConstants.DB_DATA_NOT_AVAILABLE_FOR_GIVEN_LAT_LGN;
                       msg="No record available for give lat,lng";
                   }
                }
                else {
                    Log.d(TAG, "getLast HotelList last record");
                    locationInfo=dpDao.getAvailableData();
                    if(locationInfo==null)
                    {
                       ERROR_CODE=DbConstants.DB_DATA_NOT_AVAILABLE;
                        msg="No record available empty data";
                    }
                }
                Log.d(TAG, "getLast LocationInfo ");
                if (locationInfo != null) {
                    String strInfo= Utils.getGson().toJson(locationInfo);
                    Log.d(TAG, "getLast LocationInfo "+strInfo);
                    return Observable.just(strInfo);

                }
                else {
                    return Observable.error(new LocationInfoException(msg,
                            ERROR_CODE));
                }

            }
        });
    }





}
