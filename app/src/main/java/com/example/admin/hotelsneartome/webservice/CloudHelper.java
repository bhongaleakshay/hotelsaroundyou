package com.example.admin.hotelsneartome.webservice;

import android.content.Context;
import android.location.Location;
import android.util.Log;

import com.example.admin.hotelsneartome.databases.DBHelper;
import com.example.admin.hotelsneartome.databases.LocationInfo;
import com.example.admin.hotelsneartome.model.Example;
import com.example.admin.hotelsneartome.utils.Utils;
import com.example.admin.hotelsneartome.utils.WebApiConstants;

import io.reactivex.Observable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * CloudHelper to implement Webservice
 */

public class CloudHelper {



    private static String TAG=CloudHelper.class.getCanonicalName();

    /**
     * instance of WebServiceAPI interface
     */
    private static IWebServiceApi mWebApiService;

    private static int radius = 10000;



    /**
     * Used to init Retrofit service for
     * web service consumption
     */
    public static void init(Retrofit retrofit)
    {
       mWebApiService= retrofit.create(IWebServiceApi.class);
    }

    /**
     * return instance of retrofit
     * @return retrofit
     */
    public static Retrofit getRetrofit(OkHttpClient okHttpClient)
    {
        return new Retrofit.Builder()
                .baseUrl(WebApiConstants.BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }


    /**
     * Used to set http client for debugging purpose
     * @return httpclient
     */
    public static OkHttpClient getOkHttpDebugClient()
    {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        httpClient.addInterceptor(logging);
        return httpClient.build();
    }


    public static Observable<Example>
    downloadHotels(final Context context, String key, final Location location)
    {
        String strLocation= location.getLatitude()+ "," +location.getLongitude();
        return mWebApiService.getNearbyPlaces(key,strLocation,radius)
                .subscribeOn(Schedulers.io())
                .doOnNext(new Consumer<Example>() {
                    @Override
                    public void accept(Example example) throws Exception {
                        if(example!=null) {
                            if(example.getStatus()!=null) {
                                if(example.getStatus().equalsIgnoreCase(WebApiConstants.STATUS_OK)) {
                                    String latitude = String.valueOf(location.getLatitude());
                                    String logitude = String.valueOf(location.getLongitude());
                                    String json = Utils.getGson().toJson(example);
                                    LocationInfo locationInfo = new LocationInfo();
                                    locationInfo.setJson(json);
                                    locationInfo.setLatitude(latitude);
                                    locationInfo.setLongitude(logitude);
                                    Log.d("onAccept", "" + locationInfo.getJson());
                                    DBHelper.getDbHelper(context).insertVCF(locationInfo);
                                }
                                else {
                                    Log.d("onAccept", " else error");
                                }
                            }
                        }
                    }
                })
                .observeOn(Schedulers.newThread());
    }




}
