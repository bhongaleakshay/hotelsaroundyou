package com.example.admin.hotelsneartome.utils;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.ConnectivityManager;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.example.admin.hotelsneartome.R;
import com.example.admin.hotelsneartome.webservice.IWebServiceApi;
import com.google.gson.Gson;

import java.net.InetAddress;
import java.util.concurrent.Callable;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;

public class Utils {

    private static Gson gson;
    private static final int TWO_MINUTES = 1000 * 60 * 2;


    public static boolean isInternetConnected(Context context) {
        boolean isInternetConnected = false;
        try {
            ConnectivityManager connManager = (ConnectivityManager)
                    context.getSystemService(Context.CONNECTIVITY_SERVICE);
            assert connManager != null;
            if (connManager.getActiveNetworkInfo() != null && connManager.getActiveNetworkInfo()
                    .isAvailable() && connManager.getActiveNetworkInfo().isConnected()) {
                isInternetConnected = true;
            }
        } catch (Exception ex) {
            isInternetConnected = false;
        }
        return isInternetConnected;
    }

    public static Observable<Boolean> getNetworkConnectionStatus() {
        return Observable.defer(new Callable<ObservableSource<? extends Boolean>>() {
            @Override
            public ObservableSource<? extends Boolean> call() throws Exception {

                try {
                    InetAddress inetAddress = InetAddress.getByName("www.google.com");
                    boolean result = inetAddress != null && !inetAddress.toString().equals("");
                    //Logger.d("UserProfileActivity","getNetworkConnectionStatus :: "+result);
                    return Observable.just(result);
                } catch (Exception ex) {
                    ex.printStackTrace();
                    return Observable.just(false);
                }

            }
        });
    }

    public static boolean isEmpty(String s) {
        return TextUtils.isEmpty(s);
    }

    public static synchronized Gson getGson() {
        if (gson == null) {
            gson = new Gson();
        }
        return gson;
    }

    public static void loadImageUrl(Context context, ImageView view, String reference, int height, int width) {

        String PHOTO_URL = "https://maps.googleapis.com/maps/api/place/photo?photoreference=" + reference + "&sensor=false&maxheight=" +
                height + "&maxwidth=" + width + "&key=" + IWebServiceApi.API_KEY;
        RequestOptions options =
                new RequestOptions().override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL);
         /* options = options.placeholder(R.drawable.ic_preview_not_available);
          options =options.error(R.drawable.ic_preview_not_available);*/

        Glide.with(context)
                .load(PHOTO_URL)
                .apply(options)
                .into(view);
    }

    public static boolean isPermissionAllowed(Context context) {
        boolean isAllowed = true;
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                ||
                ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
            isAllowed = false;
        }
        return isAllowed;
    }


    public static boolean isBetterLocation(Location location, Location currentBestLocation) {
        if (currentBestLocation == null) {
            // A new location is always better than no location
            return true;
        }
        // Check whether the new location fix is newer or older
        long timeDelta = location.getTime() - currentBestLocation.getTime();
        boolean isSignificantlyNewer = timeDelta > TWO_MINUTES;
        boolean isSignificantlyOlder = timeDelta < -TWO_MINUTES;
        boolean isNewer = timeDelta > 0;
        // If it's been more than two minutes since the current location, use the new location
        // because the user has likely moved
        if (isSignificantlyNewer) {
            return true;
            // If the new location is more than two minutes older, it must be worse
        } else if (isSignificantlyOlder) {
            return false;
        }
        // Check whether the new location fix is more or less accurate
        int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 200;
        // Check if the old and new location are from the same provider
        boolean isFromSameProvider = isSameProvider(location.getProvider(),
                currentBestLocation.getProvider());
        // Determine location quality using a combination of timeliness and accuracy
        if (isMoreAccurate) {
            return true;
        } else if (isNewer && !isLessAccurate) {
            return true;
        } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
            return true;
        }
        return false;
    }

    /**
     * Checks whether two providers are the same
     */
    private static boolean isSameProvider(String provider1, String provider2) {
        if (provider1 == null) {
            return provider2 == null;
        }
        return provider1.equals(provider2);
    }

    public static String getErrorMsg(int errorCode) {
        String msg = "Something went wrong";
        switch (errorCode) {
            case WebApiConstants.CODE_OVER_QUERY_LIMIT:
                msg = " Reached to maximum query limit";
                break;

            case WebApiConstants.CODE_ZERO_RESULTS:
                msg = "No result Found";
                break;

            case WebApiConstants.CODE_REQUEST_DENIED:
                msg = "Request denied";
                break;

            case WebApiConstants.CODE_INVALID_REQUEST:
                msg = "Invalid request";
                break;

            case WebApiConstants.CODE_UNKNOWN_ERROR:
                msg = "Unknown error";
                break;
            case DbConstants.DB_DATA_NOT_AVAILABLE:
                msg="Please Enable mobile data,location and get hotels around you";
                break;
            case DbConstants.DB_DATA_NOT_AVAILABLE_FOR_GIVEN_LAT_LGN:
                msg="No Data available for your current location";
                break;
        }

        return msg;
    }


}