package com.example.admin.hotelsneartome.ui;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.example.admin.hotelsneartome.R;
import com.example.admin.hotelsneartome.utils.AppConstants;
import com.example.admin.hotelsneartome.utils.IAlertListener;
import com.example.admin.hotelsneartome.utils.Utils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;

import java.util.ArrayList;

public abstract class BaseActivity extends AppCompatActivity {


    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    public abstract void onPermissionGrant();
    public abstract void playServiceNotUpdated();



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(checkPlayServices()) {
            if(!Utils.isPermissionAllowed(this))
            {
                askForPermission();
            }
        }
        else {
            playServiceNotUpdated();
        }
    }

    protected void  askForPermission()
    {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.ACCESS_FINE_LOCATION) &&
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.ACCESS_COARSE_LOCATION)) {
            Log.d("base","activity");
            showAlertBox("Permission are denied by user,please enable it",
                    getString(R.string.label_yes),
                    getString(R.string.label_no), new IAlertListener() {
                        @Override
                        public void onYesPressed() {
                            loadSettingScreen();
                        }

                        @Override
                        public void onNoPressed() {
                            finish();
                        }
                    });

        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION},
                    AppConstants.PERMISSION_CODE);
        }

    }

    private void loadSettingScreen()
    {
        try {
            Intent intent = new Intent();
            intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            Uri uri = Uri.fromParts("package", getPackageName(), null);
            intent.setData(uri);
            startActivityForResult(intent, AppConstants.PERMISSION_SETTING);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("base","onActivityResult"+requestCode+" "+resultCode);
        if(requestCode== AppConstants.PERMISSION_SETTING)
        {
            if(Utils.isPermissionAllowed(this))
            {
                onPermissionGrant();
            }
            else
            {
                askForPermission();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode)
        {
            case AppConstants.PERMISSION_CODE:
                if(grantResults.length>0)
                {
                    boolean isAllPermissionGranted=true;
                    for(int i:grantResults)
                    {
                        if(i!= PackageManager.PERMISSION_GRANTED)
                        {
                            isAllPermissionGranted=false;
                        }
                    }
                    if(!isAllPermissionGranted)
                    {
                        askForPermission();
                    }
                    else
                    {
                        onPermissionGrant();
                    }
                }
                break;

        } // end of switch
    }// end of onRequest Permission Result




    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);

        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST);
            } else {
                finish();
            }

            return false;
        }

        return true;
    }

    public void showAlertBox(String message, String yesLabel, String noLabel,
                             final IAlertListener iAlertListener)
    {
        try {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setMessage(message);

            final AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, yesLabel,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {
                            if (iAlertListener != null) {
                                iAlertListener.onYesPressed();
                                alertDialog.dismiss();
                            }
                        }
                    });
            alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, noLabel,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (iAlertListener != null) {
                                iAlertListener.onNoPressed();
                                alertDialog.dismiss();
                            }
                        }
                    });

            alertDialog.show();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
