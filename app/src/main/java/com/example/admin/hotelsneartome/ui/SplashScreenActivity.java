package com.example.admin.hotelsneartome.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.Toast;

import com.example.admin.hotelsneartome.R;
import com.example.admin.hotelsneartome.utils.AppConstants;
import com.example.admin.hotelsneartome.utils.Utils;

public class SplashScreenActivity extends BaseActivity {

    @Override
    public void onPermissionGrant() {
        loadHomeScreen();
    }

    @Override
    public void playServiceNotUpdated() {
        Toast.makeText(getApplicationContext(), "Play service not updated", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        if(Utils.isPermissionAllowed(getApplicationContext()))
        {
            loadHomeScreen();
        }
    }

    private void loadHomeScreen()
    {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(getApplicationContext(),HomeActivity.class));
                finish();
            }
        }, AppConstants.SPLASH_TIME);
    }
}
