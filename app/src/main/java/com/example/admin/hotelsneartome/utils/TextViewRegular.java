package com.example.admin.hotelsneartome.utils;

import android.content.Context;
import android.util.AttributeSet;

/**
 * this class is used to set custom regular font for text view
 */

public class TextViewRegular extends android.support.v7.widget.AppCompatTextView {
    public TextViewRegular(Context context) {
        super(context);
        initFont(context);
    }

    public TextViewRegular(Context context, AttributeSet attrs) {
        super(context, attrs);
        initFont(context);
    }

    public TextViewRegular(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initFont(context);
    }

    /**
     * to set custom font to textView
     *
     * @param context context of view
     */
    private void initFont(Context context) {
        setTypeface(FontFace.getFontFace(context).getFontRegular());
    }
}
