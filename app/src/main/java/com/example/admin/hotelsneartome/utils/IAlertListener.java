package com.example.admin.hotelsneartome.utils;

/**
 * Used  to setAlert Listener for ui
 */

public interface IAlertListener {

    void onYesPressed();
    void onNoPressed();
}
