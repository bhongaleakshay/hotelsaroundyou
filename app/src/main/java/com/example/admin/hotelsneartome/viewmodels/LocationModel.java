package com.example.admin.hotelsneartome.viewmodels;

import android.arch.lifecycle.LiveData;
import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.example.admin.hotelsneartome.utils.Utils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;

public class LocationModel  extends LiveData<Location> implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {


    private static LocationModel instance;
    private GoogleApiClient googleApiClient;
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private LocationRequest mLocationRequest;
    private static final long UPDATE_INTERVAL = 5000, FASTEST_INTERVAL = 5000; // = 5 seconds
    private String TAG=LocationModel.class.getCanonicalName();

    private Location mLastLocation=null;



    // lists for permissions
    public static LocationModel getInstance() {
        if (instance == null) {
            instance = new LocationModel();
        }
        return instance;
    }

    private void createLocationRequest()
    {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
    }

    public LocationRequest getLocationRequest() {
        return mLocationRequest;
    }

    private LocationModel() {
        createLocationRequest();
    }

    public synchronized void initGoogleApiClient(Context appContext) {
        Log.d(TAG, "Build google api client");
        createLocationRequest();
        googleApiClient = new GoogleApiClient.Builder(appContext)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }
    @Override
    protected void onActive() {
        googleApiClient.connect();
    }

    @Override
    protected void onInactive() {
        if (googleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(
                    googleApiClient, this);
        }
        googleApiClient.disconnect();
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
        startLocationUpdates();
    }
    private void startLocationUpdates() {

        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, mLocationRequest, this);
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.w(TAG, "On Connection suspended " + i);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult result) {
        Log.e(TAG, "GoogleApiClient connection has failed " + result);
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d(TAG, "received: ");

        if(mLastLocation==null)
        {
            mLastLocation=location;
            Log.d(TAG, "received: location null if");
            postValue(location);
        }
        else {
            Log.d(TAG, "received: location null else ");
            if (Utils.isBetterLocation(mLastLocation, location)) {
                Log.d(TAG, "received: if ::");
                mLastLocation = location;
                postValue(location);
            } else {
                Log.d(TAG, " received: else ::" );
            }
        }

    }


}
