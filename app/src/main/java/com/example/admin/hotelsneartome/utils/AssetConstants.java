package com.example.admin.hotelsneartome.utils;


/**
 * Constants for Asset
 * FontFace Constants.
 */
public interface AssetConstants {


    String FONT_REGULAR = "font/lato_regular.ttf";

    String FONT_LIGHT="font/lato_light.ttf";

    String FONT_SEMI_BOLD ="font/lato_semibold.ttf";

    String FONT_MEDIUM="font/lato_medium.ttf";

    String FONT_BOLD="font/lato_bold.ttf";

}
